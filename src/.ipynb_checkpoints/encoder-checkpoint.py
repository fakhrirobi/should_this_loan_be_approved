import pandas as pd 
from collections import Counter
import joblib 

class feature_encoding() : 
    def __init__(self,data,classification_target) :
        self.data = data
        self.classification_target = classification_target
        
    def reduce_uniqueness(self,columns,save_feature_name=True,threshold=0.75) : 
        for col in columns : 
            row_limit = int(threshold*len(self.data[col]))
            feature_values = []
            row_count = 0 
            counts=Counter(self.data[col])
            for i,j in  counts.most_common() :
                row_count += dict(counts)[i]
                feature_values.append(i)
                if row_count >= row_limit : 
                    break
            feature_values.append('Other')
            filename = f'src/{col}_values.joblib'
            joblib.dump(feature_values,filename)
            self.data.loc[:,col] = self.data.loc[:,col].apply(lambda x : x if x in feature_values else 'Other')
        return self.data
    
    def target_feature_encoding(self) : 
        self.data.loc[:,self.classification_target] = self.data.loc[:,self.classification_target].map({'P I F':1, 'CHGOFF':0})
        return self.data
    
    def ordinal_encoding(self,target_cols) : 
        self.data
        
    def one_hot_encoding(self,target_cols,reduce=False) : 
        
        if reduce == False : 
            copy_df =  self.data.copy()
            isolate_data = pd.get_dummies(copy_data[target_cols])
            dropped_data = copy_data.drop(target_cols,axis=1)
            encoded_data = pd.concat([dropped_data,isolate_data],axis=1)
            return encoded_data
        else : 
            copy_data = self.reduce_uniqueness(columns=target_cols)
            isolate_data = pd.get_dummies(copy_data[target_cols])
            dropped_data = copy_data.drop(target_cols,axis=1)
            encoded_data = pd.concat([dropped_data,isolate_data],axis=1)
            return encoded_data
            

        
             
            
        
    def begin_encoding(self) : 
        self.target_feature_encoding()
        return self.data

data = pd.read_csv('dataset/cleaned_data.csv')

encoder = feature_encoding(data,'MIS_Status')

data = encoder.begin_encoding()
cols = ['Bank','City','State','BankState']
data = encoder.one_hot_encoding(cols,reduce=True)

data.drop('Zip',axis=1,inplace=True)

data.to_csv('ready_to_scale.csv',index=False)

data.FranchiseCode.value_counts()