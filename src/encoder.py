import pandas as pd 
from collections import Counter
import joblib 
import logging

#custom mapper dictionary (saved from .ipynb notebook)
map_city = joblib.load('mapable/map_City.joblib')
map_state = joblib.load('mapable/map_State.joblib')
map_bank_state = joblib.load('mapable/map_BankState.joblib')
map_sector = joblib.load('mapable/map_Sector.joblib')
map_bank = joblib.load('mapable/map_Bank.joblib')

def reduce_uniqueness(data,columns,threshold=0.75) : 
    ''' 
    This function is aimed to reduce cardinality of certain columns in dataset by 75% threshold
    Parameters : 
    data -> dataframe format of dataset
    threshold -> default 0.75 
    
    '''
    for col in columns : 
        row_limit = int(threshold*len(data[col]))
        feature_values = []
        row_count = 0 
        counts=Counter(data[col])
        for i,j in  counts.most_common() :
            row_count += dict(counts)[i]
            feature_values.append(i)
            if row_count >= row_limit : 
                break
        feature_values.append('Other')
        data.loc[:,col] = data.loc[:,col].apply(lambda x : x if x in feature_values else 'Other')

    return data

def one_hot_encoding(data,cols) : 
    ''' 
    this function use pd.get_dummies and bit modification to concatenate back the data
    Parameters : 
    data -> dataframe format of dataset 
    cols -> 1 or more features to drop  
    '''
    copy_data = data.copy()
    drop_columns = copy_data.drop(cols,axis=1)
    isolated_data = copy_data[cols]
    isolated_data = pd.get_dummies(isolated_data)
    encoded_data = pd.concat([drop_columns,isolated_data],axis=1)
    return encoded_data

def custom_map(data,col) : 
    ''' 
    custom mapper of for categorical variables that with high cardinality
    
    '''
    import joblib
    mapper = joblib.load(f'mapable/map_{col}.joblib')
    data.loc[:,col] = data.loc[:,col].map(mapper)
    
    
def encode_feature(data) : 
    '''the flow process of feature encoding
    Parameters : 
    data -> dataframe format of dataset
    '''
    
    print('Feature Encoding is on Process')
    #encode MIS_Status (target variable)
    data.loc[:,'MIS_Status'] = data.loc[:,'MIS_Status'].map({"P I F":1,"CHGOFF":0})    
    print('Successfully encode :  MIS_Status')
    
    #one hot encoding feature 
    cols_to_hot_encode = ['IsNewBusiness','IsFranchise','IsUrban','RevLineCr','LowDoc','BackedRealEstate']
    data = one_hot_encoding(data,cols_to_hot_encode)
    print('Successfully encode : \n IsNewBusiness , IsFranchise, IsUrban, RevLineCr,LowDoc, BackedRealEstate ')
    
    #since there are lot of cardinalities in City and Bank we need to reduce its uniqueness
    col_to_reduce_cardinality = ['City','Bank']
    data  = reduce_uniqueness(data,col_to_reduce_cardinality)
    
    #Mapping  Encoding on ['City','State','Bank','BankState','Sector'] 
    col_to_custom_map = ['City','State','Bank','BankState','Sector'] 
    for col in col_to_custom_map : 
        custom_map(data,col)
        print(data[col].unique)
        
    col_to_drop = ["payment_deadline","NAICS","default_rate_state","default_rate_sector","ApprovalDate","DisbursementDate","LoanNr_ChkDgt" , "Name" , "Zip", 'relative_delta']
    data.drop(col_to_drop,axis=1,inplace=True)
    
    
    print('Successfully encode : \n City , State, Bank, BankState,Sector ')
    
    return data 