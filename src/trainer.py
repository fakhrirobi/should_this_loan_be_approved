from sklearn.model_selection import (train_test_split,
                                     StratifiedKFold)

from sklearn.ensemble import VotingClassifier
from sklearn import  metrics

import xgboost as xgb 
import catboost as cb 
import numpy as np 
import logging

def cross_validation_score(model,X,Y) : 
    print(f'Cross Validation for {type(model).__name__} is on process')
    ''' 
    Parameters : 
    list_of_models -> list of models if the model only concist of one still 
    you have to make it as a list  
    X_TRAIN -> Features
    Y_TRAIN -> Target 
    '''
    X = X.values
    avg_f1 = list()
    avg_auc = list()
    cv = StratifiedKFold(n_splits=5)
    for idx,(train_idx,test_idx) in enumerate(cv.split(X,Y)) : 
        X_TRAIN = X[train_idx]
        Y_TRAIN = Y[train_idx]
        X_VAL = X[test_idx]
        Y_VAL = Y[test_idx]
        model.fit(X_TRAIN,Y_TRAIN)
        prediction = model.predict(X_VAL)
        #prediction for predict proba-> AUC purpose 
        preds = model.predict_proba(X_VAL)[:,1]
        #showing the score of each model 
        #f1 score since the target class is imbalanced 
        f1_ = metrics.f1_score(Y_VAL,prediction)
        avg_f1.append(f1_)
        # auc
        auc_ = metrics.roc_auc_score(Y_VAL,preds)
        avg_auc.append(auc_)
    print(f'Cross Validation for {type(model).__name__} is done')
    print(f'cross validation report \n : {type(model).__name__} has f1 score = {np.mean(avg_f1)} and auc score ={np.mean(avg_auc)}')


def training_process(data,target_cols='MIS_Status') : 
    print('Training Process Begin')

    X  = data.drop(target_cols,axis=1)
    y  = data.loc[:,target_cols].values
    
    
    X_TRAIN,X_TEST,Y_TRAIN,Y_TEST = train_test_split(X,y,test_size=0.2)

    model = xgb.XGBClassifier()
    cross_validation_score(model,X_TRAIN,Y_TRAIN)
    model.fit(X_TRAIN,Y_TRAIN)
    prediction = model.predict(X_TEST)
    preds = model.predict_proba(X_TEST)
    
    
    f1_result = metrics.f1_score(Y_TEST,prediction)
    print(f'f1 score {type(model).__name__} on TEST SET : \n {f1_result}')
    auc_result = metrics.roc_auc_score(Y_TEST,preds[:,1])
    print(f'roc auc score {type(model).__name__} on TEST SET  : \n {auc_result}')
    
    #saving xgb model 
    import joblib
    joblib.dump(model,'model/xgb_model.joblib')
    print(f'model {type(model).__name__} has been saved')
    
    #voting classifier model 
    xgb_model = xgb.XGBClassifier()
    cb_model = cb.CatBoostClassifier()
    
    vote_model = VotingClassifier(estimators=[('xgb',xgb_model),('cb',cb_model)],voting='soft')
    # cross_validation_score(vote_model,X_TRAIN,Y_TRAIN) still has issue with ValueError: The estimator module should be a classifier.
    vote_model.fit(X_TRAIN,Y_TRAIN)
    
    vote_prediction = vote_model.predict(X_TEST)
    vote_preds = vote_model.predict_proba(X_TEST)
    
    f1_result_vote = metrics.f1_score(Y_TEST,vote_prediction)
    print('Voting Classifier result')
    print(f'f1 score {type(vote_model).__name__} on TEST SET : \n {f1_result_vote}')
    auc_result_vote = metrics.roc_auc_score(Y_TEST,vote_preds[:,1])
    print(f'roc auc score {type(vote_model).__name__} on TEST SET  : \n {auc_result_vote}')
    
    
    #saving vote_model
    joblib.dump(vote_model,'model/vote_model_cb_xgb.joblib')
    print(f'model {type(vote_model).__name__} has been saved')
    print('Training Process is Done')