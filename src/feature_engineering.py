import pandas as pd 
import numpy as np 
import logging

sector_map = {
    '11' : 'Agriculture,forestry,fishing,and hunting', 
    '21' : 'Mining,quarrying,and oil and gas extraction', 
    '22' : 'Utilities', 
    '23' : 'Construction', 
    '31' : 'Manufacturing','32' : 'Manufacturing','33' : 'Manufacturing' , 
    '42' : 'Wholesale Trade' , 
    '44' : 'RetailTrade' , '45' : 'RetailTrade', 
    '48' : 'Transportation and Warehousing' , '49' : 'Transportation and Warehousing', 
    '51' : 'Information', '52' : 'Finance and Insurance' , 
    '53' : 'Real Estate, Rental, and Leasing' , 
    '54' : 'Professional, scientific, and technical services', 
    '55' : 'Management of companies and enterprises' , 
    '56' : 'Administrative and support and waste management and remediation services',
    '61' : 'Educational services' , 
    '62' : 'Health care and social assistance' , 
    '71' : 'Arts, entertainment, and recreation' , 
    '72' : 'Accommodation and food services' , 
    '81' : 'Other services (edataept public administration)',
    '92' : 'Public administration'
}

default_map_sector = {
    '21' : 8 , 
    '11' : 9 , 
    '55' : 10 , '62' : 10 , 
    '22' : 14 , 
    '92' : 15 , 
    '54' : 19 , '42' : 19 , '31' : 19 , 
    '32' : 16,
    '33' : 14 , 
    '81' : 20 , 
    '71' : 22 , 
    '72' : 22 , 
    '44' : 22 , 
    '45' : 23 , 
    '23' : 23 , 
    '56' : 24 , 
    '61' : 24 , 
    '51' : 25 , 
    '48' : 27 , 
    '49' : 23 , 
    '52' : 28 , 
    '53' : 29 , 
    
}



def additional_feature(data) : 
    ''' function description'''
    
    #create Sector feature
    data.loc[:,'Sector'] = data.loc[:,'NAICS'].map(sector_map)
    print('Successfully create new feature : Sector')
    
    #create sector default rate 
    data.loc[:,'default_rate_sector'] = data.loc[:,'NAICS'].map(default_map_sector)/100
    print('Successfully create new feature : default_rate_sector')
    
    
    #creating state default rate 
    # we are going to create another feature => default rate of each state 
    #default rate could be defined by pct CHGOFF from total loan in each state
    # group first the state and MIS_Status
    mis_group_states= data.groupby(['State', 'MIS_Status'])['State'].count().unstack('MIS_Status')
    # grouping rsult multiindex -> so we need to convert to single index 
    mis_group_states.columns = mis_group_states.columns.get_level_values(0)
    #create feature of default rate of each state
    mis_group_states['DefaultRate'] = mis_group_states['CHGOFF'] / (mis_group_states['CHGOFF']+mis_group_states['P I F'])
    default_rate_state = mis_group_states.loc[:,['DefaultRate']].to_dict()['DefaultRate']
    # map the default rate 
    data.loc[:,'default_rate_state'] = data.loc[:,'State'].map(default_rate_state)
    print('Successfully create new feature : default_rate_state')
    
    #creating pct_risk_default feature 
    data.loc[:,'pct_risk_default'] = data.loc[:,'default_rate_state'] + data.loc[:,'default_rate_sector']
    print('Successfully create new feature : pct_risk_default')
    
    
    #creating is_backed_realestate
    def is_backed_realEstate(length) : 
        if length >= 240 : 
            return "Y"
        else : 
            return "N"
    
    data.loc[:,'BackedRealEstate'] = data.loc[:,'Term'].apply(lambda x : is_backed_realEstate(x))
    print('Successfully create new feature : is_backed_realestate')
    
    
    #creating SBA_guaranted_pct feature
    data['SBA_guaranted_pct'] = data['SBA_Appv'] / data['GrAppv']
    print('Successfully create new feature : SBA_guaranted_pct')
    
    #creating isInecession Feature 
    from dateutil.relativedelta import relativedelta

    # based on the guideline again the dataset has long time frame from 70s to 2014
    # and we have been encountered recessions so of course in recession era loan tend to become more unpaid
    # we are going to classify whether a loan is between recession date 
    # first create deadline payment as point of active loan 
    # converting term in months to relative ( to add date format with disbursment date)
    data.loc[:,'relative_delta'] = data.loc[:,'Term'].apply(lambda x : pd.DateOffset(months=x))
    data.loc[:,'payment_deadline'] = data.loc[:,'relative_delta'] + data.loc[:,'DisbursementDate']
    recession_begin = pd.Timestamp(2007, 12, 31)
    recession_end = pd.Timestamp(2009, 6, 30)
    
    def is_in_recession(date) : 
        if date <= recession_end and date >= recession_begin : 
            return 1
        elif date == np.nan : 
            return np.nan 
        else : 
            return 0
    data.loc[:,'IsInRecession'] = data.loc[:,'payment_deadline'].apply(lambda x : is_in_recession(x))
    print('Successfully create new feature : IsInRecession')
    
    
    print('FEATURE ENGINEERING PROCESS IS DONE')
    return data 



