import yaml
import pandas as pd 
import logging
import numpy as np 

date_columns = ["ApprovalDate",'ChgOffDate', 'DisbursementDate']
currency_features = [ 'DisbursementGross','BalanceGross', 'ChgOffPrinGr', 'GrAppv', 'SBA_Appv']

def preprocessing(data) : 
    ''' 
    This function is created in order to simplify the data preprocessing 
    
    
    
    '''
    #changing string to datetime
    for col in date_columns : 
        data.loc[:,col] = pd.to_datetime(data.loc[:,col])
    print('Successfully changed the format of date columns')
    #NAICS feature handling
    def map_NAICS(text) : 
        'this function was created to pick 2 first digit in order for latter sector classification ' 
        text = text[:2]
        if text == '0' : 
            return np.nan
        else : 
            return text
    data.loc[:,'NAICS'] = data.loc[:,'NAICS'].astype('str').apply(lambda x : map_NAICS(x))
    print('Successfully casted NAICS feature')
    
    #Handling ApprovalFY feature
    data.loc[:,'ApprovalFY'].replace({'1976A':1976},inplace=True)
    data.loc[:,'ApprovalFY'] = data.loc[:,'ApprovalFY'].astype('int')
    print('Successfully casted the ApprovalFY feature')
    
    
    #Handling Currency Feature 
    data.loc[:,currency_features] = data.loc[:,currency_features].replace('[\$,]', '', regex=True).astype(float)
    print('Succesfully casted currency features')
    
    #Handling LowDoc Feature 
    def cast_LowDoc(text) : 
        to_be_casted = [ 'C', '1', 'S', 'R', 'A', '0']
        if text == np.nan : 
            return np.nan
        elif text not in to_be_casted : 
            return text
        else : 
            return np.nan
    
    data.loc[:,'LowDoc'] = data.loc[:,'LowDoc'].apply(lambda x : cast_LowDoc(x))
    print('Successfully casted LowDoc Feature')
    
    #Handling RevLineCr 
    def cast_RevLineCr(text) : 
                to_be_casted = [ '0', 'T', '`', ',', '1', 'C', '3', '2', 'R', '7',
                                'A', '5', '.', '4', '-', 'Q']
                if text == np.nan : 
                    return np.nan
                elif text not in to_be_casted : 
                    return text
                else : 
                    text = np.nan
                    return text
    data.loc[:,'RevLineCr'] = data.loc[:,'RevLineCr'].apply(lambda x : cast_RevLineCr(x))
    print('Successfully casted RevLineCr Feature')
    
    #Handling NewExist Feature
    data.loc[:,'NewExist'] = data.loc[:,'NewExist'].replace({1.0: "N", 2.0:"Y", 0:np.nan})
    data.rename(columns={'NewExist' : 'IsNewBusiness'},inplace=True)
    print('Successffully casted NewExist feature')
    
    #Handling FranchiseCode 
    def cast_FranchiseCode(text) : 
        if text == '0' or text == '1' : 
            return 'N' 
        else : 
            return 'Y'
    
    data.loc[:,'FranchiseCode'] = data.loc[:,'FranchiseCode'].astype('str').apply( lambda x : cast_FranchiseCode(x))
    data.rename(columns={'FranchiseCode' : 'IsFranchise'},inplace=True)
    print('Successfully casted the Franchise Code Feature')
    
    #Handling UrbanRural Feature 
    data.rename(columns={'UrbanRural' : 'IsUrban'},inplace=True)
    data.loc[:,'IsUrban'] = data.loc[:,'IsUrban'].replace({1.0: "Y", 2.0:"N", 0:np.nan})
    print('Successfully casted UrbanRural Feature')
    
    return data 
    
def handling_missing_values(data) : 
    
    
    #handling missing LowDocs 
    data.loc[(data['LowDoc'].isnull()) & (data['DisbursementGross'] < 150000), "LowDoc"] = "Y"
    data.loc[(data['LowDoc'].isnull()) & (data['DisbursementGross'] >= 150000), "LowDoc"] = "N"
    print('Successfully handle missing values on LowDoc')
    
    #handling missing NAICS 
    mode_NAICS = data.loc[:,'NAICS'].mode()[0]
    data.loc[:,'NAICS'].fillna(mode_NAICS,inplace=True)
    print('Successfully handle missing values on NAICS')
    
    #Handling Bank State Missing Values 
    bank_mode = data.loc[:,'Bank'].mode()[0]
    data.loc[:,'Bank'] = data.loc[:,'Bank'].fillna(bank_mode)
    # we are going to impute bank state by first mode of BankState from first mode of Bank Name
    data.loc[data['BankState'].isnull()==True, "BankState"] = data[data.Bank == data.Bank.mode()[0]]['BankState'].mode()[0]
    print('Successfully handle missing values on Bank and BankState')
        

    #Handling Missing City and State 
    codified_city_state = {}
    for st in data.State.unique() : 
        codified_city_state[st] = set(data[data.State==st]['City'].to_list())
    #filling missing state by refering to its city from codified_city_state
    for k,v in codified_city_state.items() : 
        for city in data[data.State.isnull()==True]['City'] :
            #checking if city in missing state has similarity 
            if set(city).intersection(v) is not None : 
                data.loc[ (data["State"].isnull()==True), "State"] = k
                print('State Imputed')
            else : 
                continue
            
    for state in data[data.City.isnull()==True]['State'] : 
        city_mode = data.loc[data.State==state]['City'].mode()[0]
        data.loc[ (data["City"].isnull()==True), "City"] = city_mode
    print('Successfully handle missing values on City and State')
    
    #Handling Missing Value on IsBusiness 
    data.loc[:,'IsNewBusiness'].fillna(data.loc[:,'IsNewBusiness'].mode()[0],inplace=True)
    print('Successfully handle missing values on IsNewBusiness')
    
    #Handling Missing Values on RevLineCr
    data.loc[:,'RevLineCr'].fillna(data.loc[:,'RevLineCr'].mode()[0],inplace=True)
    print('Successfully handle missing values on RevLineCr')
    
    #Handling Missing Values on IsUrban
    data.loc[:,'IsUrban'].fillna(data.loc[:,'IsUrban'].mode()[0],inplace=True)
    print('Successfully handle missing values on IsUrban')
    
    #Dropping na on some features, reason -> cant imput missing values 
    data.dropna(subset=['MIS_Status','DisbursementDate','Name'],inplace=True)
    data.drop('ChgOffDate',axis=1,inplace=True)
    print(data.isnull().sum())
    return data 
    
    
    
