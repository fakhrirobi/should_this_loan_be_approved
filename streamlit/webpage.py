

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}
# 4 Page WebApp, consist of : 
# 1. Project Explanation 
# 2. EDA 
# 3. Training Flow - Show the Output 
# 4. Custom Model hyper parameter 
# 5. Prediction Section 
sidebar = html.Div(
    [
        html.H3("Sidebar", className="display-4"),
        html.Hr(),
        html.P(
            "A simple sidebar layout with navigation links", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Project Explanation", href="/", active="exact"),
                dbc.NavLink("Exploratory Data Analysis", href="/page-1", active="exact"),
                dbc.NavLink("Training Flow", href="/page-2", active="exact"),
                dbc.NavLink("Create Custom Model Hyperparameter", href="/page-3", active="exact"),
                dbc.NavLink("Try Prediction", href="/page-4", active="exact")
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)
homepage = html.Div(
    [html.H2(" Hi")]
) 
page_1 = None
page_2 = None

import joblib

bank = joblib.load('bank.joblib')

from sklearn.preprocessing import LabelEncoder
option_bank= []

for val in bank : 
    dict_col = {'label':val,'value':val}
    option_bank.append(dict_col)


bank[0]
page_4 = html.Div([dbc.Row(
    dbc.Col(dcc.Checklist(
    options=option_bank,
    value=[bank[0]]
))
    
    
    )],className='PredictionSection')
content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def page_controller(pathname):
    if pathname == "/":
        #replace with app.layout
        #render MD file + pict 
        #return homepage
        return html.P("This is the content of the home page!")
    elif pathname == "/page-1":
        #//TODO : RENDER EDA CHART -> prereuisites -> eda data
        #replace with app.layout
        #return page_1 
        return homepage
    elif pathname == "/page-2":
        #replace with app.layout
        return html.P("Oh cool, this is page 2!")
    
    elif pathname == "/page-3":
        #replace with app.layout
        return html.P("Oh cool, this is page 3!")
    
    elif pathname == "/page-4":
        #replace with app.layout
        return page_4
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


if __name__ == "__main__":
    app.run_server(port=8888)