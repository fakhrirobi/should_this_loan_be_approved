import streamlit as st 
import pandas as pd 
import numpy as np 
import urllib
import joblib
import os 
page_navigation = st.sidebar.selectbox('Page Navigation',options=['Project Description',
                                                          'Exploratory Data Analysis',
                                                          'Prediction'])

mappable_path = os.path.join(os.getcwd(),'mapable')

map_city = joblib.load(os.path.join(mappable_path,'map_City.joblib'))
map_state = joblib.load(os.path.join(mappable_path,'map_State.joblib'))
map_bank = joblib.load(os.path.join(mappable_path,'map_Bank.joblib'))
map_bank_state = joblib.load(os.path.join(mappable_path,'map_BankState.joblib'))
map_sector = joblib.load(os.path.join(mappable_path,'map_Sector.joblib'))

if page_navigation == 'Project Description' : 
    url = 'https://raw.githubusercontent.com/fakhrirobi/travel_insurance_webapp/main/README.md'
    def get_file_content_as_string(url):
        #for reading readme.md from github
        response = urllib.request.urlopen(url)
        return response.read().decode("utf-8")
    st.markdown(get_file_content_as_string(url),unsafe_allow_html=True) 
    
elif page_navigation == 'Exploratory Data Analysis' : 
    st.write(os.getcwd()) 
elif page_navigation == 'Prediction' : 
    with st.form('Fill The Data') : 
        name = st.text_input('Input Name')
        City = st.selectbox('Select City',options=(x for x in map_city.keys()))
        State = st.selectbox('Select State',options=(x for x in map_state.keys()))
        Bank = st.selectbox('Select Bank',options=(x for x in map_bank.keys()))
        BankState = st.selectbox('Select Bank State',options=(x for x in map_bank_state.keys()))
        ApprovalYear = st.number_input('Input Approval Year',min_value=1962,value=1990)
        Term = st.number_input('Input Duration of Loan Term (Month)',min_value=0,value=0)
        NoEmp  = st.number_input('Input Number of Company Employee',min_value=0,value=0)
        createjob = st.number_input('Input Number of Jobs Created',min_value=0,value=0)
        
else : 
    print('No Choice Bro')

No