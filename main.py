
import repackage
repackage.up()
import pandas as pd 
import numpy as np 
import yaml
from tqdm import tqdm

from src.preprocess import handling_missing_values, preprocessing
from src.encoder import (map_city,map_bank,
                         map_bank_state,map_state,map_sector,
                         reduce_uniqueness,one_hot_encoding,custom_map,encode_feature)


from src.feature_engineering import additional_feature

from src.trainer import training_process,cross_validation_score



CONFIG_PATH = 'config/config.yaml'


#reading config.yaml file 
with open(CONFIG_PATH) as file:
    config = yaml.safe_load(file)


def start() :
    with tqdm(total=100) as pbar : 
        data = pd.read_csv(config['datapath']) 
        data = preprocessing(data)
        pbar.update(20)
        data = handling_missing_values(data)
        pbar.update(20)
        data = additional_feature(data)
        pbar.update(20)
        data = encode_feature(data)
        data.head(2).to_csv('sample/sample_head.csv',index=False)
        pbar.update(20)
        training_process(data)
        pbar.update(20)
    

if __name__ == '__main__' : 
    start()









